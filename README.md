# Feedback To Grey Software

I joined the **Open Source Web Essentials Certificate at Grey Software** considering my semester 25 hours site project to work with professionals.

I came to know about Grey Software from my senior and further explored its website and projects on github. I luckily met Arsala Khan the president of Grey Software.

Arsala Khan has great communication skills and well informed about technology. He is ninja in motivation and I received always a positive reponse that helped me with friendly communication.

I would definitely mention that I improved alot with my technical skills, got confidence to hands-on new technology and got a chance to bring my knowledge into implementation.

Grey Software is build on a great idea to help students work on open source technologies. Comparing to the free learning content platforms on Internet, Grey Software is doing beyond that by eliminating the distraction and hurdles that one face during self-learning.

#### It was a nice 25 hours experience at Grey Software.
